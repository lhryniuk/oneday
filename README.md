# oneday

Task management system

## Idea

Oneday is based on a way I'm planning my work and focuses on planning long-term
things like long tutorials (like [Learn OpenGL](https://learnopengl.com/)),
watching series of videos ([Krita tutorial by
GDquest](https://www.youtube.com/playlist?list=PLhqJJNjsQ7KE3FLHIE31UgmLdcqsZfXTw))
or reading huge books with independent chapters step by step (like [AOSA
book](http://aosabook.org/en/index.html)). Basic time unit is one week (without
weekend, which is used as a buffer). Main problem I've faced was being detached
by "spontaneous" events like a conference or a travel and losing a track of
progress. I'm a type of person that works on several different things "at once" and doing steady progress without right tool was too cumbersome.

## Workflow

1. Break a project into parts that can be done within one day.
2. Add them to the week, from Monday to Friday and assign a size to each.
3. Work and learn.
4. After each week remaining tasks are moved to the next week.

## Task format

This project is also an approach to provide simple tool that fits Unix
philosophy, especially that's easy to connect with different programs (e.g.
website or tray applet). It's supposed to work locally with a possibility of
deploying it on some external host. Oneday uses JSON to describe tasks:

```json
{
    "id": "task identifier",
    "name": "description of a thing to do",
    "day": "date, when it's supposed to be done",
    "size": "1/2/3 relative size of a task (limit would"
            "be e.g. 5 per day to manage them automatically)"
}
```

There's no "status" field as I'm not interested in things I've already done,
only current workload per day and week. It should be easy to integrate it with a
different tools, e.g. for estimation and progress graphs, but that's not a goal
of Oneday.

## HTTP API

### `GET /`

Returns a **list** of the **current week** tasks.

### `GET /tasks`

Returns a **list** of all tasks.

### `GET /tasks/{id}`

Returns a **single** task with a given `id`.

### `POST /tasks`

Adds a task to the list. Accepts JSON, sample:

```json
{
    "name": "Task name",
    "date": "2011-10-05T14:48:00.000Z",
    "size": 2
}
```


## MIT License

Copyright 2018 Łukasz Hryniuk

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.