package assert

func Empty(e error) {
	if e != nil {
		panic(e)
	}
}
