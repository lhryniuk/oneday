package csv

import (
	"encoding/csv"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/hryniuk/oneday/internal/pkg/assert"
	"github.com/hryniuk/oneday/internal/pkg/tasks"
)

func dumps(t tasks.Task) []string {
	dateBytes, err := t.Date.MarshalText()
	if err != nil {
		log.Fatal(err)
	}

	return []string{strconv.Itoa(int(t.ID)), t.Name, string(dateBytes[:]), strconv.Itoa(int(t.Size))}
}

type File struct {
	Path string
}

func (f *File) Save(l tasks.List) error {
	fw, err := os.Create(f.Path)
	assert.Empty(err)

	w := csv.NewWriter(fw)

	tasksStrings := make([][]string, len(l.Tasks))

	for i, task := range l.Tasks {
		tasksStrings[i] = dumps(task)
	}

	return w.WriteAll(tasksStrings)
}

func (f *File) Load() (tasks.List, error) {
	l := tasks.NewList()

	fr, err := os.Open(f.Path)
	assert.Empty(err)

	r := csv.NewReader(fr)

	// TODO: read one by one to handle big files
	records, err := r.ReadAll()
	assert.Empty(err)

	for _, record := range records {
		id, err := strconv.Atoi(record[0])
		if err != nil {
			log.Fatal(err)
		}
		name := record[1]
		date := time.Now()
		err = date.UnmarshalText([]byte(record[2]))
		if err != nil {
			log.Fatal(err)
		}
		size, err := strconv.Atoi(record[3])
		if err != nil {
			log.Fatal(err)
		}

		l.Append(tasks.Task{int32(id), name, date, int8(size)})
	}

	return l, nil
}
