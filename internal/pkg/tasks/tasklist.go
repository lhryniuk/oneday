package tasks

import (
	"time"
)

var nextID int32

func init() {
	nextID = 0
}

// Task represents the single thing that user is supposed to do on a given Date.
type Task struct {
	// TODO: after saving and restarting ID will be duplicated
	// find a better id generation algorithm (UUID?)
	ID   int32
	Name string    `json:"name"`
	Date time.Time `json:"date"`
	Size int8      `json:"size"`
}

func New(name string, date time.Time, size int8) Task {
	return Task{nextID, name, date, size}
}

type List struct {
	Tasks []Task
}

func NewList() List {
	return List{make([]Task, 0)}
}

type Saver interface {
	Save(l List) error
}

type Loader interface {
	Load() (List, error)
}

func (l *List) Append(t Task) {
	l.Tasks = append(l.Tasks, t)
	nextID++
}
