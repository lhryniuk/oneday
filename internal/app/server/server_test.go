package server

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/hryniuk/oneday/internal/pkg/tasks"
)

var (
	port = "8080"
)

type IOStub struct {
}

func (s *IOStub) Save(tasks.List) error {
	return nil
}

func (s *IOStub) Load() (tasks.List, error) {
	return tasks.NewList(), nil
}

func TestReturnsEmptyArrayOnEmptyTaskList(t *testing.T) {
	s := New(&IOStub{}, &IOStub{})

	expected := []byte("[]")

	req, err := http.NewRequest("GET", buildUrl("/tasks"), nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()

	s.allTasks(res, req)

	if res.Code != http.StatusOK {
		t.Errorf("Response code was %v; want 200", res.Code)
	}

	if bytes.Compare(expected, res.Body.Bytes()) != 0 {
		t.Errorf("Response body was '%v'; want '%v'", res.Body, expected)
	}
}

func TestAddTaskToTaskList(t *testing.T) {
	s := New(&IOStub{}, &IOStub{})

	expected := []byte("")

	reader := strings.NewReader(`
	{
		"name": "Test task",
		"size": 3,
		"date": "2018-01-02T15:06:05+07:00"
	}`)

	req, err := http.NewRequest("POST", buildUrl("/tasks"), reader)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()

	s.allTasks(res, req)

	if res.Code != http.StatusOK {
		t.Errorf("Response code was %v; want 200", res.Code)
	}

	if bytes.Compare(expected, res.Body.Bytes()) != 0 {
		t.Errorf("Response body was '%v'; want '%v'", res.Body, expected)
	}

	if len(s.taskList.Tasks) == 0 {
		t.Error("Task was not added to the list after POST request")
	}
}

func buildUrl(path string) string {
	url, _ := url.Parse("")
	url.Scheme = "http"
	url.Host = fmt.Sprintf("%s:%s", "localhost", port)
	url.Path = path
	return fmt.Sprint(url)
}
