package server

import "net/http"

func (s *Server) routes() {
	http.HandleFunc("/", s.currentWeek)
	http.HandleFunc("/tasks.html", s.htmlView)
	http.HandleFunc("/tasks", s.allTasks)
}
