package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/golang/go/src/pkg/html/template"
	"github.com/hryniuk/oneday/internal/pkg/assert"
	"github.com/hryniuk/oneday/internal/pkg/tasks"
	"github.com/hryniuk/oneday/internal/pkg/utils"
)

type Server struct {
	taskList tasks.List
	saver    tasks.Saver
	loader   tasks.Loader
}

func New(saver tasks.Saver, loader tasks.Loader) Server {
	taskList, err := loader.Load()
	assert.Empty(err)

	return Server{taskList, saver, loader}
}

func (s *Server) currentWeek(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		now := time.Now()
		currentWeekTasks := make([]tasks.Task, 0)
		for _, task := range s.taskList.Tasks {
			if utils.IsSameWeek(task.Date, now) {
				currentWeekTasks = append(currentWeekTasks, task)
			}
		}
		json, err := json.Marshal(currentWeekTasks)
		assert.Empty(err)
		w.Write(json)
	default:
		fmt.Fprintf(w, "Sorry, only GET method is supported.")
	}
}

func (s *Server) htmlView(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("tasklist.html")
	assert.Empty(err)
	t.Execute(w, s.taskList.Tasks)
}

func (s *Server) allTasks(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		json, err := json.Marshal(s.taskList.Tasks)
		assert.Empty(err)
		w.Write(json)
	case "POST":
		decoder := json.NewDecoder(r.Body)

		var t tasks.Task
		err := decoder.Decode(&t)
		assert.Empty(err)

		s.taskList.Append(tasks.New(t.Name, t.Date, t.Size))
		err = s.saver.Save(s.taskList)
		assert.Empty(err)
	default:
		fmt.Fprintf(w, "Sorry, only GET and POST methods are supported.")
	}
}

func (s *Server) Start(taskList tasks.List, port, taskListPath string) error {
	s.routes()
	return http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
}
