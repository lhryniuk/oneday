package main

import (
	"log"

	"github.com/hryniuk/oneday/internal/app/server"
	"github.com/hryniuk/oneday/internal/pkg/tasks"
	"github.com/hryniuk/oneday/internal/pkg/tasks/csv"
)

// Switch to sqlite (?)
const taskListPath = "./tl.csv"
const port = "8080"

var taskList tasks.List

func main() {
	s := server.New(&csv.File{taskListPath}, &csv.File{taskListPath})
	log.Fatal(s.Start(taskList, port, taskListPath))
}
