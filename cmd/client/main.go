package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/hryniuk/oneday/internal/pkg/tasks"

	"github.com/jroimartin/gocui"
)

const serverHost = "http://localhost:8080/tasks"

var names []string

func init() {
	names = []string{"monday", "tuesday", "wednesday", "thursday", "friday"}
}

func getCurrentWeek() tasks.List {
	r, err := http.Get(serverHost)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}

	var l tasks.List

	err = json.Unmarshal(body, &l.Tasks)
	if err != nil {
		log.Fatal(err)
	}

	return l
}

func updateLayoutEvery(d time.Duration, g *gocui.Gui) {
	for range time.Tick(d) {
		go g.Update(func(g *gocui.Gui) error {
			l := getCurrentWeek()
			for i, n := range names {
				v, _ := g.View(n)
				v.Clear()

				for _, t := range l.Tasks {
					if d := t.Date.Weekday(); int(d) == i+1 {
						_, err := fmt.Fprintf(v, "%s\n", t.Name)
						if err != nil {
							log.Fatal(err)
						}
					}
				}
			}

			return nil
		})
	}
}

func main() {
	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()

	g.SetManagerFunc(layout)

	go updateLayoutEvery(time.Second, g)

	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		log.Panicln(err)
	}
}

func layout(g *gocui.Gui) error {
	paneWidth := 20
	paneHeight := 20

	for i := 0; i < len(names); i++ {
		if v, err := g.SetView(names[i], i*paneWidth, 0, (i+1)*paneWidth-2, paneHeight); err != nil {
			v.Title = strings.ToTitle(names[i])
			if err != gocui.ErrUnknownView {
				return err
			}
		}
	}

	return nil
}

func quit(_ *gocui.Gui, _ *gocui.View) error {
	return gocui.ErrQuit
}
