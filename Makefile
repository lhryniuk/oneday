.PHONY: default
default: test build

.PHONY: build
build:
	go build ./cmd/server
	go build ./cmd/client

.PHONY: test
test:
	go test ./internal/app/server
